import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FormEditProject from "../Components/Forms/FormEditProject";
import { Layout, Table, Button, message, Input } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { headerProject } from "./utility";
import { OPEN_DRAWER_WITH_FORM } from "../redux/const/const";
import { ProjectService } from "../service/classProjectServices";

let projectServiceClass = new ProjectService();

const ProjectManagementPage = () => {
  const [projectListWClass, setProjectListWClass] = useState([]);
  const { Content } = Layout;
  const { update } = useSelector((state) => state.drawerReducer);
  const dispatch = useDispatch();

  const fetchProjectWithClass = () => {
    projectServiceClass
      .getProject()
      .then((res) => {
        let newList = res.data.content.map((project) => {
          return {
            ...project,
            action: (
              <>
                <Button
                  onClick={() => {
                    handleEditProjectWithClass(project.id);
                  }}
                  className="bg-blue-600"
                >
                  <EditOutlined className="text-white" />
                </Button>
                <Button
                  onClick={() => {
                    handleDeleteProject(project.id);
                  }}
                  className="bg-red-500"
                >
                  <DeleteOutlined className="text-white" />
                </Button>
              </>
            ),
          };
        });

        setProjectListWClass(newList);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEditProjectWithClass = (id) => {
    projectServiceClass
      .getProjectDetail(id)
      .then((res) => {
        dispatch({
          type: OPEN_DRAWER_WITH_FORM,
          payload: <FormEditProject data={res.data.content} />,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleDeleteProject = (projectID) => {
    projectServiceClass
      .deleteProject(projectID)
      .then((res) => {
        fetchProjectWithClass();
        message.success("Xoa thanh cong!");
      })
      .catch((err) => {
        fetchProjectWithClass();
        message.error(err.response.data.content);
      });
  };

  const handleSearch = (e) => {
    let { value } = e.target;
    if (!value) {
      fetchProjectWithClass();
    }
    projectServiceClass
      .searchProject(value)
      .then((res) => {
        console.log(res);
        let mappedResponse = res.data.content.map((project, index) => {
          return {
            ...project,
            action: (
              <Fragment key={index}>
                <Button
                  onClick={() => {
                    handleEditProjectWithClass(project.id);
                  }}
                  className="bg-blue-600"
                >
                  <EditOutlined className="text-white" />
                </Button>
                <Button
                  onClick={() => {
                    handleDeleteProject(project.id);
                  }}
                  className="bg-red-500"
                >
                  <DeleteOutlined className="text-white" />
                </Button>
              </Fragment>
            ),
          };
        });
        setProjectListWClass(mappedResponse);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  useEffect(() => {
    fetchProjectWithClass();

  }, [update]);

  return (
    <Layout>
      <Content
        className="site-layout"
        style={{
          padding: "0 50px",
        }}
      >
        <div
          className="container"
          style={{
            padding: 24,
            minHeight: 380,
            background: "#fff",
          }}
        >
          <Input
            placeholder="Enter project name to find"
            className="my-5 placeholder:text-black"
            onChange={handleSearch}
          />

          <Table dataSource={projectListWClass} columns={headerProject} />
        </div>
      </Content>
    </Layout>
  );
};
export default ProjectManagementPage;
