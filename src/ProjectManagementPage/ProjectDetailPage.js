import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { message } from "antd";
import { ProjectService } from "../service/classProjectServices";

export default function ProjectDetailPage() {
  let classProjectServices = new ProjectService();
  let { projectId } = useParams();
  let [projectDetail, setProjectDetail] = useState({});

  useEffect(() => {
    classProjectServices
      .getProjectDetail(projectId)
      .then((res) => {
        setProjectDetail(res.data.content);
        console.log(projectDetail);
      })
      .catch((err) => {
        message.error(err.response.data.message);
      });
  }, [projectId]);

  const renderMember = (obj) => {
    return obj.map((item) => {
      return (
        <div className="w-48 h-60">
          <img src={item.avatar} />
          <span>{item.name}</span>;
        </div>
      );
    });
  };

  return (
    <div className="flex flex-row">{renderMember(projectDetail.members)}</div>
  );
}
