import { NavLink } from "react-router-dom";
import { Avatar, Tooltip } from "antd";

export const headerProject = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
    sorter: (a, b) => a.id - b.id,
    sortDirections: ["descend"],
  },
  {
    title: "PROJECT NAME",
    dataIndex: "projectName",
    width: 150,
    key: "projectName",
    render: (text, record, index) => (
      <NavLink key={index} to={`/project-details/${record.id}`}>
        {text}
      </NavLink>
    ),
    sorter: (a, b) => {
      let item1 = a.projectName?.trim().toLowerCase();
      let item2 = b.projectName?.trim().toLowerCase();
      if (item1 < item2) {
        return -1;
      }
      return 1;
    },
  },
  {
    title: "CATEGORY NAME",
    dataIndex: "categoryName",
    key: "categoryName",
    width: 150,
    sorter: (a, b) => {
      let item1 = a.categoryName?.trim().toLowerCase();
      let item2 = b.categoryName?.trim().toLowerCase();
      if (item1 < item2) {
        return -1;
      }
      return 1;
    },
  },
  {
    title: "CREATOR",
    dataIndex: "creator",
    render: (creator) => <p>{creator.name}</p>,
    key: "creator",
    width: 150,
  },
  {
    title: "MEMBER",
    dataIndex: "members",

    key: "member",
    width: 150,
    render: (members) => {
      return (
        <Avatar.Group
          maxCount={2}
          size="large"
          maxStyle={{
            color: "#f56a00",
            backgroundColor: "#fde3cf",
          }}
        >
          {members.map((member) => {
            return (
              <Tooltip title={member.name} placement="top">
                <Avatar
                  style={{
                    borderRadius: "50%",
                  }}
                  icon={<img alt="User avatar" src={member.avatar} />}
                />
              </Tooltip>
            );
          })}
        </Avatar.Group>
      );
    },
  },
  {
    title: "ACTION",
    dataIndex: "action",
    key: "action",
    width: 150,
  },
];
