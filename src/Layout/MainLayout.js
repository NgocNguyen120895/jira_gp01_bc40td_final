import React from "react";
import HeaderComponent from "../Components/Header/HeaderComponent";
import FooterComponent from "../Components/Footer/FooterComponent";
import DrawerModal from "../Components/Modal/DrawerModal";

export default function MainLayout({ Component }) {
  return (
    <div>
      <HeaderComponent />
      <DrawerModal />
      {Component}
      <FooterComponent />
    </div>
  );
}
