import React from "react";
import { Drawer } from "antd";
import { useDispatch, useSelector } from "react-redux";

export default function DrawerModal() {
  const dispatch = useDispatch();
  const onClose = () => {
    dispatch({ type: "CLOSE_DRAWER", payload: false });
  };

  const { visible, drawerContent } = useSelector(
    (state) => state.drawerReducer
  );

  return (
    <>
      <Drawer
        width={600}
        onClose={onClose}
        open={visible}
        bodyStyle={{
          paddingBottom: 80,
        }}
      >
        {drawerContent}
      </Drawer>
    </>
  );
}
