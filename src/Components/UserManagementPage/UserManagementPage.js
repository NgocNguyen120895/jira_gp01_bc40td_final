import React, { useEffect, useState } from "react";
import { Layout, Table, theme, Button } from "antd";
import { headerUser } from "./utility";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { UserServices } from "../../service/classUserServices";

export default function UserManagementPage() {
  const { Content } = Layout;
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [userList, setUserList] = useState([]);

  let userServiceClass = new UserServices();

  const fetchUserList = () => {
    userServiceClass
      .getUserList()
      .then((res) => {
        let userArray = res.data.content.map((user) => {
          return {
            ...user,
            action: (
              <>
                <Button className="bg-blue-600">
                  <EditOutlined className="text-white" />
                </Button>
                <Button className="bg-red-500">
                  <DeleteOutlined className="text-white" />
                </Button>
              </>
            ),
          };
        });
        setUserList(userArray);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchUserList();
  }, []);

  return (
    <Layout>
      <Content
        className="site-layout"
        style={{
          padding: "0 50px",
        }}
      >
        <div
          className="container"
          style={{
            padding: 24,
            minHeight: 380,
            background: colorBgContainer,
          }}
        >
          <Table dataSource={userList} columns={headerUser} />
        </div>
      </Content>
    </Layout>
  );
}
