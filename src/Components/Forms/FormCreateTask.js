import React, { useEffect, useRef, useState } from "react";
import { Button, Form, Input, Select, Slider, message } from "antd";
import { Editor } from "@tinymce/tinymce-react";
import { TaskServices } from "../../service/taskServices";
import { userService } from "../../service/userServices";
import { ProjectService } from "../../service/classProjectServices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { CLOSE_DRAWER } from "../../redux/const/const";

const { Option } = Select;
export default function FormCreateTask() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [project, setProject] = useState([]);
  const [taskType, setTaskType] = useState([]);
  const [priority, setPriority] = useState([]);
  const [members, setMembers] = useState([]);
  let [description, setDescription] = useState("");
  let [projectId, setProjectId] = useState();
  const [timeTracking, setTimeTracking] = useState({
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
  });

  let classProjectServices = new ProjectService();
  let classTaskServices = new TaskServices();

  const getData = () => {
    classProjectServices
      .getProject()
      .then((res) => {
        setProject(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    classTaskServices
      .getPriority()
      .then((res) => {
        console.log(res);
        setPriority(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    classTaskServices
      .getTaskType()
      .then((res) => {
        console.log(res);
        setTaskType(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const getMemberByProjectId = (projectId) => {
    userService
      .getUserByProjectId(projectId)
      .then((res) => {
        setMembers(res.data.content);
      })
      .catch((err) => {
        setMembers([]);
      });
  };

  const handleSearchAssigner = (searchQuery) => {
    members
      .filter((item) => item.name.toLowerCase().includes(searchQuery))
      .map((member) => {
        return {
          value: member.userId,
          label: member.name,
        };
      });
  };

  const handleOnEditorChange = (content) => {
    setDescription(content);
  };

  const handleOnSubmit = (value) => {
    let obj = { ...value, description, projectId };
    classTaskServices
      .createTask(obj)
      .then((res) => {
        message.success("Tạo task thành công!");
        dispatch({
          type: CLOSE_DRAWER,
        });
        navigate("/projectManagementPage");
      })
      .catch((err) => {
        message.error(`Tạo task thất bại. ${err.response.data.message}`);
      });
  };

  let userOption = members.map((item) => {
    return { label: item.name, value: item.userId };
  });

  useEffect(() => {
    getData();
  }, []);
  const editorRef = useRef(null);

  return (
    <div className="container">
      <h2 className="text-xl font-medium text-center">Create Task</h2>
      <Form
        layout="vertical"
        className="flex flex-col space-y-1"
        onFinish={handleOnSubmit}
      >
        <Form.Item
          name="projectName"
          label="Project"
          rules={[
            {
              required: true,
              message: "Please select a project!",
            },
          ]}
        >
          <Select
            placeholder="Please select a project"
            onSelect={(e) => setProjectId(e)}
            onChange={(value) => getMemberByProjectId(value)}
          >
            {project.map((project, index) => {
              return (
                <Option key={index} value={project.id}>
                  {project.projectName}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item
          name="taskName"
          label="Task name"
          rules={[{ required: true, message: "Please enter your task name" }]}
        >
          <Input />
        </Form.Item>
        <div className="grid grid-cols-3 space-x-3">
          <Form.Item
            label="Status"
            name="statusId"
            rules={[
              {
                required: true,
                message: "Please select a status!",
              },
            ]}
          >
            <Select>
              <Option value="1">BACKLOG</Option>
              <Option value="2">SELECTED FOR DEVELOPMENT</Option>
              <Option value="3">IN PROGRESS</Option>
              <Option value="4">DONE</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="priorityId"
            label="Priority"
            rules={[
              {
                required: true,
                message: "Please select priority!",
              },
            ]}
          >
            <Select>
              {priority.map((priority, index) => {
                return (
                  <Option key={index} value={priority.priorityId}>
                    {priority.description}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            name="typeId"
            label="Task Type"
            rules={[
              {
                required: true,
                message: "Please select task type!",
              },
            ]}
          >
            <Select>
              {taskType.map((taskType, index) => {
                return (
                  <Option key={index} value={taskType.id}>
                    {taskType.taskType}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
        </div>

        <Form.Item name="listUserAsign" label="Assigners">
          <Select
            mode="multiple"
            placeholder="please select"
            size="middle"
            options={userOption}
            onSearch={(value) => {
              handleSearchAssigner(value);
            }}
          />
        </Form.Item>

        <div className="grid grid-cols-3">
          <h2 className="col-span-3 text-center mt-3 font-medium">
            Time tracking
          </h2>
          <Slider
            className="col-span-3"
            tooltip={{
              open: true,
            }}
            value={timeTracking.timeTrackingSpent}
            max={
              Number(timeTracking.timeTrackingSpent) +
              Number(timeTracking.timeTrackingRemaining)
            }
          />

          <Form.Item
            name="timeTrackingSpent"
            label="Time Tracking Spent"
            rules={[
              {
                required: true,
                message: "Please enter time!",
              },
            ]}
          >
            <Input
              name="timeTrackingSpent"
              className="input_createTask"
              type="number"
              Input
              min={0}
              onChange={(e) => {
                setTimeTracking({
                  ...timeTracking,
                  timeTrackingSpent: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item
            name="timeTrackingRemaining"
            label="Time Tracking Remaining"
            rules={[
              {
                required: true,
                message: "Please enter time!",
              },
            ]}
          >
            <Input
              type="number"
              initialValues={0}
              min={0}
              onChange={(e) => {
                setTimeTracking({
                  ...timeTracking,
                  timeTrackingRemaining: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item label="Estimate">
            <Input className="input_createTask" />
          </Form.Item>
        </div>
        <Form.Item name="description" label="Description"></Form.Item>

        <Editor
          onEditorChange={handleOnEditorChange}
          onInit={(evt, editor) => (editorRef.current = editor)}
          initialValue=""
          init={{
            height: 200,
            menubar: false,
            plugins: [
              "advlist autolink lists link image charmap print preview anchor",
              "searchreplace visualblocks code fullscreen",
              "insertdatetime media table paste code help wordcount",
            ],
            toolbar:
              "undo redo | formatselect | " +
              "bold italic backcolor | alignleft aligncenter " +
              "alignright alignjustify | bullist numlist outdent indent | " +
              "removeformat | help",
            content_style:
              "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
          }}
        />

        <Form.Item label="">
          <Button htmlType="submit">Submit</Button>
        </Form.Item>
      </Form>
    </div>
  );
}
