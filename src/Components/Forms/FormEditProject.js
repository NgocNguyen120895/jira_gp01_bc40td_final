import React, { useState } from "react";
import { Button, Form, Input, Select, message } from "antd";
import { useEffect } from "react";
import { Option } from "antd/es/mentions";
import { useDispatch } from "react-redux";
import { CLOSE_DRAWER } from "../../redux/const/const";
import { Editor } from "@tinymce/tinymce-react";
import TextArea from "antd/es/input/TextArea";
import { ProjectService } from "../../service/classProjectServices";

export default function FormEditProject({ data }) {
  const [arrProjectCategory, setArrProjectCategory] = useState([]);
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  let classProjectServices = new ProjectService();

  useEffect(() => {
    loadProfile();
    classProjectServices
      .getCategory()
      .then((res) => {
        setArrProjectCategory(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [data]);

  const onFinish = (values) => {
    let newValues = { ...values, creator: "0" };
    classProjectServices
      .updateProject(newValues.id, newValues)
      .then((res) => {
        console.log(res);
        message.success(res.data.message);
        dispatch({ type: CLOSE_DRAWER });
        classProjectServices.getProjects();
      })
      .catch((err) => {
        console.log(err);
        classProjectServices.getProjects();
      });
  };

  const loadProfile = () => {
    form.setFieldsValue({
      projectName: data.projectName,
      id: data.id,
      description: data.description,
      categoryId: data.projectCategory.id,
    });
  };

  return (
    <>
      <h3 className="font-medium text-xl text-center">Update Project</h3>
      <Form
        form={form}
        layout="vertical"
        style={{
          maxWidth: 600,
        }}
        onFinish={onFinish}
      >
        <Form.Item name="id" label="Project ID">
          <Input disabled="true" />
        </Form.Item>
        <Form.Item name="projectName" label="Project Name">
          <Input />
        </Form.Item>
        <Form.Item name="categoryId" label="Project Category">
          <Select>
            {arrProjectCategory?.map((item, index) => {
              return (
                <Option key={index} value={item.id}>
                  {item.projectCategoryName}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item name="description" label="TextArea">
          <TextArea />
        </Form.Item>
        <Form.Item>
          <Button className="bg-blue-500 text-white" htmlType="submit">
            Update
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
