import React from "react";
import { localServices } from "../../service/localService/localLoginService";
import { Form, Input, Button } from "antd";

export default function UserDetails() {
  let detail = localServices.get();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  return (
    <div className="container">
      <Form layout="vertical" onFinish={onFinish}>
        <Form.Item label="User Id">
          <Input disabled defaultValue={detail.id} />
        </Form.Item>
        <Form.Item label="Email" name="email">
          <Input defaultValue={detail.email} />
        </Form.Item>
        <Form.Item label="Name" name="name">
          <Input defaultValue={detail.name} />
        </Form.Item>
        <Form.Item label="Phone number" name="phoneNumber">
          <Input defaultValue={detail.phoneNumber} />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="passWord"
          label="Confirm Password"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Please confirm your password!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("The two passwords that you entered do not match!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button type="default" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
