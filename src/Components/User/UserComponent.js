import React from "react";
import { useSelector } from "react-redux";
import { SettingOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu } from "antd";
import { localServices } from "../../service/localService/localLoginService";
import { NavLink } from "react-router-dom";

export default function UserComponent() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  const items = [
    {
      label: (
        <h2
          className="text-md font-bold text-slate-500"
          style={{ cursor: "default" }}
        >
          ATLASSIAN ADMIN
        </h2>
      ),
      key: "0",
      disabled: true,
    },
    {
      label: <a href="/usersManagement">USER MANAGEMENT</a>,
      key: "1",
    },
    {
      label: (
        <h2
          className="text-md font-bold text-slate-500"
          style={{ cursor: "default" }}
        >
          JIRA SETTINGS
        </h2>
      ),
      key: "2",
      disabled: true,
    },
    {
      label: <a href="/projectsManagement">PROJECTS</a>,
      key: "3",
    },
  ];

  const userAction = (props) => {
    let userDropdownACtions;
    return (userDropdownACtions = [
      {
        label: (
          <h2 className="text-lg font-bold text-slate-500">Hello, {props}</h2>
        ),
        key: "0",
      },
      {
        label: (
          <NavLink to="/edit-detail" className="w-full">
            <Button type="text" className="w-full">
              Edit Details
            </Button>
          </NavLink>
        ),
        key: "1",
      },
      {
        label: (
          <Button
            className="bg-red-600 text-white w-full"
            onClick={handleUserLogout}
          >
            Logout
          </Button>
        ),
        key: "2",
      },
    ]);
  };

  const handleUserLogout = () => {
    localServices.remove();
    window.location.href = "/loginPage";
  };
  let renderContent = () => {
    if (userInfo) {
      const menu1 = <Menu items={items} />;
      const menu2 = <Menu items={userAction(userInfo.name)} />;

      return (
        <div className="flex justify-around">
          <Dropdown
            className="mt-auto mx-auto"
            overlay={menu1}
            trigger={["click"]}
          >
            <a onClick={(e) => e.preventDefault()}>
              <SettingOutlined className="text-3xl" />
            </a>
          </Dropdown>

          <Dropdown className="m-3" overlay={menu2} trigger={["click"]}>
            <a onClick={(e) => e.preventDefault()}>
              <img
                style={{ width: 40, height: 35, borderRadius: "50%" }}
                src={userInfo.avatar} alt="User avatar"
              />
            </a>
          </Dropdown>
        </div>
      );
    }
  };

  return <>{renderContent()}</>;
}
