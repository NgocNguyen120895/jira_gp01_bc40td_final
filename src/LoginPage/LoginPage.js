import React from "react";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../redux/const/const";
import { Button, Form, Input, message } from "antd";
import {
  MailOutlined,
  LockOutlined,
  TwitterOutlined,
  LinkedinOutlined,
} from "@ant-design/icons";
import bgImg from "../assets/images/techImg.jpg";
import { NavLink, useNavigate } from "react-router-dom";
import { localServices } from "../service/localService/localLoginService";
import { UserServices } from "../service/classUserServices";

export default function LoginPage() {

  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinish = (values) => {
    let userServiceClass = new UserServices();

    userServiceClass
      .userLogin(values)
      .then((res) => {
        localServices.set(res.data.content);
        navigate("/projectsManagement");
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        message.success("Đăng nhập thành công!");
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thất bại, sai email hoặc mật khẩu!");
      });
  };

  return (
    <div className="flex flex-row justify-around" style={{ height: "100vh" }}>
      <div
        style={{
          width: "100%",
          height: "100%",
          backgroundImage: `url("${bgImg}")`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
        }}
      ></div>

      <div
        style={{ width: "100%", height: "100vh" }}
        className="grid justify-center content-center"
      >
        <h1 className="font-thin text-center py-5 text-4xl">LOGIN</h1>
        <Form
          className="text-center"
          name="basic"
          style={{
            width: 300,
            maxWidth: 800,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Email is required!",
              },
            ]}
          >
            <Input size="large" prefix={<MailOutlined />} placeholder="email" />
          </Form.Item>

          <Form.Item
            name="passWord"
            rules={[
              {
                required: true,
                message: "Password is required!",
              },
            ]}
          >
            <Input.Password
              size="large"
              prefix={<LockOutlined />}
              placeholder="password"
            />
          </Form.Item>

          <Form.Item>
            <Button
              className="bg-blue-700 text-white h-10"
              style={{
                width: 300,
                maxWidth: 800,
              }}
              htmlType="submit"
            >
              Login
            </Button>
            <div>
              Dont have an account yet?{" "}
              <a className="text-blue-700" href="/registerPage">
                Register now
              </a>
            </div>
          </Form.Item>
        </Form>
        <div className="flex justify-center py-5">
          <NavLink to="https://twitter.com/twister">
            <TwitterOutlined style={{ fontSize: "48px", color: "#08c" }} />
          </NavLink>
          <NavLink to="https://www.linkedin.com/">
            <LinkedinOutlined style={{ fontSize: "48px", color: "#08c" }} />
          </NavLink>
        </div>
      </div>
    </div>
  );
}
