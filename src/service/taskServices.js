import { BaseServices } from "./baseServices";

export class TaskServices extends BaseServices {
  getTaskType() {
    return super.get(`https://jiranew.cybersoft.edu.vn/api/TaskType/getAll`);
  }

  getPriority() {
    return super.get(`https://jiranew.cybersoft.edu.vn/api/Priority/getAll`);
  }

  getTaskDetail(taskId) {
    return super.get(
      `https://jiranew.cybersoft.edu.vn/api/Project/getTaskDetail?taskId=${taskId}`
    );
  }

  createTask(task) {
    return super.post(
      `https://jiranew.cybersoft.edu.vn/api/Project/createTask`,
      task
    );
  }
}
