import axios from "axios";
import { headersConfig } from "./config";

export class BaseServices {
  put(url, data) {
    return axios({
      url,
      method: "PUT",
      data,
      headers: headersConfig(),
    });
  }

  get(url) {
    return axios({
      url,
      method: "GET",
      headers: headersConfig(),
    });
  }

  post(url, data) {
    return axios({
      url,
      method: "POST",
      data,
      headers: headersConfig(),
    });
  }

  delete(url){
    return axios({
        url,
        method: "DELETE",
        headers: headersConfig(),
      });
  }

}
