import axios from "axios";
import { BASE_URL, headersConfig } from "./config";
import { localServices } from "./localService/localLoginService";

export const userService = {
  postLogin: (userInput) => {
    return axios({
      url: `${BASE_URL}/api/Users/signin`,
      method: "POST",
      data: userInput,
      headers: {
        TokenCybersoft: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8'
      },
    });
  },

  getUserList: () => {
    console.log(localServices.get());
    return axios({
      url: `${BASE_URL}/api/Users/getUser`,
      method: "GET",
      headers: {
        TokenCybersoft: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
        Authorization: "",
      },
    });
  },

  userRegister: (userInput) => {
    return axios({
      url: `${BASE_URL}/api/Users/signup`,
      method: "POST",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8",
      },
      data: userInput,
    });
  },

  getUserByProjectId: (projectId) => {
    return axios({
      url: `${BASE_URL}/api/Users/getUserByProjectId?idProject=${projectId}`,
      method: "GET",
      headers: headersConfig(),
    });
  },
};
