import { BaseServices } from "./baseServices";
import { BASE_URL } from "./config";

export class ProjectService extends BaseServices {
  getProject = () => {
    return this.get(`${BASE_URL}/api/Project/getAllProject`);
  };

  deleteProject = (id) => {
    return this.delete(`${BASE_URL}/api/Project/deleteProject?projectId=${id}`);
  };

  getProjectDetail = (id) => {
    return this.get(`${BASE_URL}/api/Project/getProjectDetail?id=${id}`);
  };

  createProject = (data) => {
    return this.post(`${BASE_URL}/api/Project/createProjectAuthorize`, data);
  };

  getCategory = () => {
    return this.get(`${BASE_URL}/api/ProjectCategory`);
  };

  updateProject = (id, data) => {
    return this.put(
      `${BASE_URL}/api/Project/updateProject?projectId=${id}`,
      data
    );
  };

  searchProject (searchQuerry) {
    return super.get(`${BASE_URL}/api/Project/getAllProject?keyword=${searchQuerry}`)
  }

}
