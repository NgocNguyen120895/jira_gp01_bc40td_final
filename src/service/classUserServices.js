import { BaseServices } from "./baseServices";
import axios from "axios";
import { localServices } from "./localService/localLoginService";

export class UserServices extends BaseServices {
  getUserList() {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Users/getUser",
      method: "GET",
      headers: {
        Authorization: `Bearer ${localServices.get().accessToken}`,
        TokenCybersoft: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
      },
    });
  }

  userLogin(userData) {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Users/signin",
      method: "POST",
      headers: {
        // Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
        TokenCybersoft: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
      },
      data: userData,
    });
  }
  userRegister(userData) {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Users/signup",
      method: "POST",
      data: userData,
      headers: {
        TokenCybersoft: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjE0LzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDY0OTYwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0Nzk3MjAwfQ.5RzSzvDq8qA8Kfw0NePg5o7H-ZEqh0_tqOWRhEUSct8`,
      },
    });
  }

  getUserByProjectId(id) {
    return super.get(
      `https://jiranew.cybersoft.edu.vn/api/Users/getUserByProjectId?idProject=${id}`
    );
  }
}
