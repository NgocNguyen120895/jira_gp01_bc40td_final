import {
  OPEN_DRAWER,
  CLOSE_DRAWER,
  OPEN_DRAWER_WITH_FORM,
} from "./const/const";

const initialState = {
  visible: false,
  drawerContent: <p>default content</p>,
  update: false,
};

const drawerReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_DRAWER:
      return { ...state, visible: action.payload };
    case CLOSE_DRAWER:
      return { ...state, visible: false, update: true };
    case OPEN_DRAWER_WITH_FORM:
      return {
        ...state,
        visible: true,
        drawerContent: action.payload,
        update: false,
      };

    default:
      return state;
  }
};

export default drawerReducer;
