import { combineReducers } from "redux";
import projectReducer from "./projectReducer";
import userReducer from "./userReducer";
import drawerReducer from "./drawerReducer";

export const rootReducer = combineReducers({
  projectReducer,
  userReducer,
  drawerReducer,
});
