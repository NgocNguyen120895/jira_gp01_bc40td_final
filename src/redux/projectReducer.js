import {
  VIEW_ALL_CATEGORY,
  VIEW_ALL_PROJECTS,
  EDIT_PROJECT,
  GET_PROJECTDETAIL,
} from "./const/const";

let initialState = {
  projects: [],
  projectCategory: [],
  editProject: {
    id: "0",
    projectName: "string",
    creator: "0",
    description: "string",
    categoryId: "string",
  },
  projectDetail: {},
};

let projectReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_ALL_PROJECTS: {
      return {
        ...state,
        projects: payload,
      };
    }
    case VIEW_ALL_CATEGORY: {
      return {
        ...state,
        projectCategory: payload,
      };
    }
    case EDIT_PROJECT: {
      return { ...state, editProject: payload };
    }
    case GET_PROJECTDETAIL: {
      return { ...state, projectDetail: payload };
    }
    default:
      return state;
  }
};

export default projectReducer;
