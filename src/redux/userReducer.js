import { VIEW_ALL_USERS, USER_LOGIN } from "./const/const";
import { localServices } from "../service/localService/localLoginService";

let initialState = {
  userList: [],
  userInfo: localServices.get() || null,
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_ALL_USERS: {
      return {
        ...state,
        userList: payload,
      };
    }
    case USER_LOGIN: {
      return {
        ...state,
        userInfo: payload,
      };
    }
    default:
      return state;
  }
};
export default userReducer;
